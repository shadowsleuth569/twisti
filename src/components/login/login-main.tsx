import { ChangeEvent, useState } from 'react';
import { useAuth } from '@lib/context/auth-context';
import { NextImage } from '@components/ui/next-image';
import { CustomIcon } from '@components/ui/custom-icon';
import { Button } from '@components/ui/button';

// Import your custom SVG icons
import EmailIcon from './../../../public/assets/email.png';
import PassIcon from './../../../public/assets/padlock.png';
import Link from 'next/link';


export function LoginMain(): JSX.Element {
  const { signInWithGoogle, loginWithEmail } = useAuth();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleEmailSignIn = async () => {
    try {
      // Validate email and password here if needed
      if (!email || !password) {
        throw new Error('Email and password are required');
      }

      // Call the loginWithEmail function
      await loginWithEmail(email, password);
    } catch (error) {
      // Handle login error
      console.error('Login error:', error);
    }
  };

  return (
    <main className='flex min-h-screen flex-col items-center justify-center border-2 border-solid border-white'>
      {/* Water-themed background image */}

      {/* Content container */}
      <div className='flex flex-col items-center justify-center rounded-lg bg-black bg-opacity-80 p-8 backdrop-blur-md'>
        <i className='mb-4'>
          <CustomIcon
            className='h-12 w-12 text-accent-blue dark:text-twitter-icon'
            iconName='RocketLaunchIcon'
          />
        </i>
        <div className='mb-4 text-4xl font-bold text-white'>Login</div>
        <div className='mb-4 text-center text-2xl font-bold text-white'>
          Welcome to
          <br />
          Twistify World!
        </div>

        <div className='grid gap-3'>
          {/* Updated styling for form elements */}
          <div className='fade-in flex items-center rounded-lg border-2 border-b border-solid border-white p-2 text-black'>
            <NextImage
              src={EmailIcon}
              alt='Email Icon'
              width={18}
              height={18}
              className='mr-4' // Added margin-right (mr-4)
            />
            <StyledInput
              type='email'
              placeholder='Email'
              className='flex-1 border-none bg-transparent text-white outline-none'
              value={email}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setEmail(e.target.value)
              }
            />
          </div>
          <div className='fade-in flex items-center rounded-lg border-2 border-b border-solid border-white p-2 text-black'>
            <NextImage
              src={PassIcon}
              alt='Password Icon'
              width={18}
              height={18}
              className='mr-4' // Added margin-right (mr-4)
            />
            <StyledInput
              type='password'
              placeholder='Password'
              className='flex-1 border-none bg-transparent text-white outline-none'
              value={password}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setPassword(e.target.value)
              }
            />
          </div>
          <Button
            className='fade-in cursor-pointer bg-accent-blue p-1 font-bold
                      text-white transition hover:brightness-90 focus-visible:!ring-accent-blue/80 focus-visible:brightness-90 active:brightness-75'
            onClick={handleEmailSignIn}
          >
            Login
          </Button>
          {/* Divider and "o" below the Sign in button */}
          <div className='mt-4 flex items-center'>
            <div className='flex-1 border-b border-solid border-white' />
            <div className='mx-4 text-white'>o</div>
            <div className='flex-1 border-b border-solid border-white' />
          </div>
          <Button
            className='flex justify-center gap-2 border border-light-line-reply p-1 text-sm font-bold
             text-light-primary transition hover:bg-[#e6e6e6] focus-visible:bg-[#e6e6e6] active:bg-[#cccccc]
             dark:border-0 dark:bg-white dark:hover:brightness-90
             dark:focus-visible:brightness-90 dark:active:brightness-75'
            onClick={signInWithGoogle}
          >
            <CustomIcon iconName='GoogleIcon' /> Sign-In with Google
          </Button>
          <Button
            className='flex cursor-not-allowed justify-center gap-2 border border-light-line-reply p-1 text-sm
                         font-bold text-light-primary transition hover:bg-[#e6e6e6] focus-visible:bg-[#e6e6e6]
                         active:bg-[#cccccc] dark:border-0 dark:bg-white dark:hover:brightness-90 dark:focus-visible:brightness-90 dark:active:brightness-75'
          >
            <CustomIcon iconName='AppleIcon' /> Sign-In with Apple
          </Button>
          <p
            className='inner:custom-underline inner:custom-underline text-center text-xs
                         text-light-secondary inner:text-accent-blue dark:text-dark-secondary'
          >
            By signing-In, you agree to the{' '}
            <a href='' target='_blank' rel='noreferrer'>
              Terms of Service
            </a>{' '}
            and{' '}
            <a href='' target='_blank' rel='noreferrer'>
              Privacy Policy
            </a>
            , including{' '}
            <a href='' target='_blank' rel='noreferrer'>
              Cookie Use
            </a>
            .
          </p>
          <div className='text-1xl mb-4 text-white'>
            Don t have an Account?
            <Link href='/registration'>
              <a className='text-white-500 font-bold'>Sign Up</a>
            </Link>
          </div>
        </div>
      </div>
    </main>
  );
}
